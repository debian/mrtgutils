#include "util.h"
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

void print_hostname(void)
{
  char hostname[255];
  struct hostent *h;

  gethostname(hostname, sizeof(hostname));
  h = gethostbyname(hostname);
  printf("%s\n", (h ? h->h_name : "(none)"));
}

void setD(int* c,const char* name, double* ar, double i)
{
  (*c)++;
  if ((*c) > 2) {
    fprintf(stderr, "%s:error: only two parameters at a time\n", name);
    exit(2);
  }

   ar[1] = i;
   if (*c == 1) ar[0] = i;
}

