Source: mrtgutils
Section: net
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libsensors-dev,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/mrtgutils
Vcs-Git: https://salsa.debian.org/debian/mrtgutils.git

Package: mrtgutils
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 mrtg,
 mrtgutils-sensors,
Description: Utilities to generate statistics for mrtg
 MRTGutils is a collection of simple utilities to generate output useful for
 mrtg. Many of the existing mrtg setups use shell or perl scripts to gather
 output. On busy systems, these scripts can generate a lot of extra load. These
 (small) C programs can return the given statistics more efficiently.
 .
 This package provides the following binaries that return:
  - mrtg-load: the current load average (5-minute average)
  - mrtg-ip-acct: the number of IP packets that have traversed an interface
  - mrtg-apache: the number of hits to a Apache web site

Package: mrtgutils-sensors
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 mrtgutils,
Conflicts:
 mrtgutils (<= 0.7),
Description: Utilities to generate statistics for mrtg (from lm-sensors)
 MRTGutils is a collection of simple utilities to generate output useful for
 mrtg. Many of the existing mrtg setups use shell or perl scripts to gather
 output. On busy systems, these scripts can generate a lot of extra load. These
 (small) C programs can return the given statistics more efficiently.
 .
 This package provides the binary mrtg-sensors that returns data sensors
 information from lm-sensors.
