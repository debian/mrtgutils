#ifndef UPTIME_H
#define UPTIME_H

void loadavg (double *, double *, double *);
int uptime(double *uptime_secs, double *idle_secs);
void print_uptime(void);

#endif
