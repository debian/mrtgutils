#include "uptime.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static int count = 0;

static void usage(int rc)
{
  fprintf(stderr, "Usage: mrtg-system [-l 1|5|15] [-m multiplier]\n");
  exit(rc);
}

static void setPos(double** pos1, double** pos2, double i)
{
  count++;
  if (count > 2) {
    fprintf(stderr, "mrtg-uptime:error: only two parameters can be sample at a time. It can be 1, 5, or 15.\n");
    exit(2);
  }

  if (count == 1) *pos1 = &i;
  if (count == 2) *pos2 = &i;
}

int main(int argc, char **argv)
{
  char* slash;
  double  z=0;
  double avg1, avg5, avg15;
  double* pos1;
  double* pos2;
  double mult [2] = {1,1};
  int c;
  int countm = 0;

  loadavg(&avg1, &avg5, &avg15);
  pos1 = &z;
  pos2=&avg5;

  slash = strrchr(argv[0], '/');
  if (slash) argv[0] = slash + 1;

  while ((c = getopt(argc, argv, "m:l:")) > 0) {
    switch (c) {
      case 'm':
        setD(&countm, argv[0], &mult[0],atof(optarg));
        break;
      case 'l':
        if (strcmp(optarg, "1") == 0)       setPos(&pos1, &pos2, avg1);
        else if (strcmp(optarg, "5") == 0)  setPos(&pos1, &pos2, avg5);
        else if (strcmp(optarg, "15") == 0) setPos(&pos1, &pos2, avg15);
        else {
          fprintf(stderr, "%s:error: unknown load average period '%s'.\nThe possible periods are 1, 5, and 15.", argv[0], optarg);
          usage(2);
        }
        break;
      default:
	usage(0);
    }
  }


  printf("%.0f\n", *pos1*mult[0]);
  printf("%.0f\n", *pos2*mult[1]);
  print_uptime();
  print_hostname();
  return 0;
}
